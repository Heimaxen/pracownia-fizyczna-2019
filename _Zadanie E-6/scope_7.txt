ANALOG
Ch 1 Scale 50mV/, Pos 15.625mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 0.0V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 1.720ms/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Period(1), Cur 10.049ms
Frequency(1), Cur 99.510Hz
Duty(1), Cur  <49.902%
Amplitude(1), Cur  >511mV

