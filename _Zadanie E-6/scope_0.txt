ANALOG
Ch 1 Scale 100mV/, Pos 248.750mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 247.500mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 1.440ms/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Period(1), Cur 10.047ms
Frequency(1), Cur 99.530Hz
Duty(1), Cur 49.279%
Amplitude(1), Cur 450mV

